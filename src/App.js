import "bootstrap/dist/css/bootstrap.min.css"

import Pizza365FetchAPI from "./components/Pizza365RestSampleApi";

import './App.css'

function App() {
  return (
    <div className="container bg-light">
      <Pizza365FetchAPI/>
    </div>
  );
}

export default App;
